﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenesAuto
{
    class Pregunta
    {
        public string nombre { get; set; }
        public string textoPreg { get; set; }
        public string[] opciones = new string[4];
        public int correctIdx { get; set; }

        public Pregunta(string txt, string opA, string opB, string opC, string opD, int idx)
        {
            this.textoPreg = txt;
            this.opciones[0] = opA;
            this.opciones[1] = opB;
            this.opciones[2] = opC;
            this.opciones[3] = opD;
            this.correctIdx = idx;
        }
            /*public void llenarPregunta (string txt, string opA, string opB, string opC, string opD, int idx)
            {
                this.textoPreg = txt;
                this.opciones[0] = opA;
                this.opciones[1] = opB;
                this.opciones[2] = opC;
                this.opciones[3] = opD;
                this.correctIdx = idx;
            }*/
        }
}
