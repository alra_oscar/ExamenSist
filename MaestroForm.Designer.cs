﻿namespace ExamenesAuto
{
    partial class MaestroForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaestroForm));
            this.panel_mtro_archivo = new System.Windows.Forms.Panel();
            this.bt_seleccionarArchivo = new System.Windows.Forms.Button();
            this.lb_numPreg = new System.Windows.Forms.Label();
            this.tB_numPreg = new System.Windows.Forms.TextBox();
            this.panel_capturar = new System.Windows.Forms.Panel();
            this.bt_Archivo = new System.Windows.Forms.Button();
            this.bt_capturar = new System.Windows.Forms.Button();
            this.lb_fechaFin = new System.Windows.Forms.Label();
            this.dateTime_fin = new System.Windows.Forms.DateTimePicker();
            this.lb_fechaInicio = new System.Windows.Forms.Label();
            this.dateTime_inicio = new System.Windows.Forms.DateTimePicker();
            this.tb_nombreExamen = new System.Windows.Forms.TextBox();
            this.lb_nombreExamen = new System.Windows.Forms.Label();
            this.panel_mtro_capturar = new System.Windows.Forms.Panel();
            this.select_correcta = new System.Windows.Forms.ComboBox();
            this.lb_captura_correcta = new System.Windows.Forms.Label();
            this.tb_captura_num = new System.Windows.Forms.TextBox();
            this.lb_captura_num = new System.Windows.Forms.Label();
            this.bt_capturar_guard = new System.Windows.Forms.Button();
            this.lb_captura_opD = new System.Windows.Forms.Label();
            this.tb_captura_opD = new System.Windows.Forms.TextBox();
            this.lb_captura_opC = new System.Windows.Forms.Label();
            this.tb_captura_opC = new System.Windows.Forms.TextBox();
            this.lb_captura_opB = new System.Windows.Forms.Label();
            this.tb_captura_opB = new System.Windows.Forms.TextBox();
            this.lb_captura_opA = new System.Windows.Forms.Label();
            this.tB_captura_opA = new System.Windows.Forms.TextBox();
            this.lb_captura_preg = new System.Windows.Forms.Label();
            this.tB_captura_preg = new System.Windows.Forms.TextBox();
            this.panel_mtro_izq = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button_logout = new System.Windows.Forms.Button();
            this.Button_revisar = new System.Windows.Forms.Button();
            this.Button_examen = new System.Windows.Forms.Button();
            this.bt_guardarExamen_archivo = new System.Windows.Forms.Button();
            this.bt_cancelar_archivo = new System.Windows.Forms.Button();
            this.bt_capturar_crearExam = new System.Windows.Forms.Button();
            this.bt_capturar_cancelar = new System.Windows.Forms.Button();
            this.panel_mtro_archivo.SuspendLayout();
            this.panel_capturar.SuspendLayout();
            this.panel_mtro_capturar.SuspendLayout();
            this.panel_mtro_izq.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_mtro_archivo
            // 
            this.panel_mtro_archivo.Controls.Add(this.bt_cancelar_archivo);
            this.panel_mtro_archivo.Controls.Add(this.bt_guardarExamen_archivo);
            this.panel_mtro_archivo.Controls.Add(this.bt_seleccionarArchivo);
            this.panel_mtro_archivo.Controls.Add(this.lb_numPreg);
            this.panel_mtro_archivo.Controls.Add(this.tB_numPreg);
            this.panel_mtro_archivo.Location = new System.Drawing.Point(240, 10);
            this.panel_mtro_archivo.Name = "panel_mtro_archivo";
            this.panel_mtro_archivo.Size = new System.Drawing.Size(484, 361);
            this.panel_mtro_archivo.TabIndex = 0;
            // 
            // bt_seleccionarArchivo
            // 
            this.bt_seleccionarArchivo.BackColor = System.Drawing.Color.Blue;
            this.bt_seleccionarArchivo.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_seleccionarArchivo.ForeColor = System.Drawing.Color.White;
            this.bt_seleccionarArchivo.Location = new System.Drawing.Point(56, 49);
            this.bt_seleccionarArchivo.Name = "bt_seleccionarArchivo";
            this.bt_seleccionarArchivo.Size = new System.Drawing.Size(187, 31);
            this.bt_seleccionarArchivo.TabIndex = 3;
            this.bt_seleccionarArchivo.Text = "Seleccionar Archivo";
            this.bt_seleccionarArchivo.UseVisualStyleBackColor = false;
            this.bt_seleccionarArchivo.Click += new System.EventHandler(this.bt_seleccionarArchivo_Click);
            // 
            // lb_numPreg
            // 
            this.lb_numPreg.AutoSize = true;
            this.lb_numPreg.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_numPreg.Location = new System.Drawing.Point(53, 108);
            this.lb_numPreg.Name = "lb_numPreg";
            this.lb_numPreg.Size = new System.Drawing.Size(190, 18);
            this.lb_numPreg.TabIndex = 1;
            this.lb_numPreg.Text = "Numero de preguntas:";
            // 
            // tB_numPreg
            // 
            this.tB_numPreg.Location = new System.Drawing.Point(272, 108);
            this.tB_numPreg.Name = "tB_numPreg";
            this.tB_numPreg.Size = new System.Drawing.Size(100, 20);
            this.tB_numPreg.TabIndex = 0;
            // 
            // panel_capturar
            // 
            this.panel_capturar.Controls.Add(this.bt_Archivo);
            this.panel_capturar.Controls.Add(this.bt_capturar);
            this.panel_capturar.Controls.Add(this.lb_fechaFin);
            this.panel_capturar.Controls.Add(this.dateTime_fin);
            this.panel_capturar.Controls.Add(this.lb_fechaInicio);
            this.panel_capturar.Controls.Add(this.dateTime_inicio);
            this.panel_capturar.Controls.Add(this.tb_nombreExamen);
            this.panel_capturar.Controls.Add(this.lb_nombreExamen);
            this.panel_capturar.Location = new System.Drawing.Point(237, 13);
            this.panel_capturar.Name = "panel_capturar";
            this.panel_capturar.Size = new System.Drawing.Size(511, 370);
            this.panel_capturar.TabIndex = 2;
            // 
            // bt_Archivo
            // 
            this.bt_Archivo.BackColor = System.Drawing.Color.Blue;
            this.bt_Archivo.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_Archivo.ForeColor = System.Drawing.Color.White;
            this.bt_Archivo.Location = new System.Drawing.Point(101, 236);
            this.bt_Archivo.Name = "bt_Archivo";
            this.bt_Archivo.Size = new System.Drawing.Size(120, 31);
            this.bt_Archivo.TabIndex = 6;
            this.bt_Archivo.Text = "Archivo";
            this.bt_Archivo.UseVisualStyleBackColor = false;
            this.bt_Archivo.Click += new System.EventHandler(this.bt_Archivo_Click);
            // 
            // bt_capturar
            // 
            this.bt_capturar.BackColor = System.Drawing.Color.Blue;
            this.bt_capturar.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_capturar.ForeColor = System.Drawing.Color.White;
            this.bt_capturar.Location = new System.Drawing.Point(287, 236);
            this.bt_capturar.Name = "bt_capturar";
            this.bt_capturar.Size = new System.Drawing.Size(120, 31);
            this.bt_capturar.TabIndex = 3;
            this.bt_capturar.Text = "Capturar";
            this.bt_capturar.UseVisualStyleBackColor = false;
            this.bt_capturar.Click += new System.EventHandler(this.Button_capturar_Click);
            // 
            // lb_fechaFin
            // 
            this.lb_fechaFin.AutoSize = true;
            this.lb_fechaFin.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_fechaFin.Location = new System.Drawing.Point(31, 147);
            this.lb_fechaFin.Name = "lb_fechaFin";
            this.lb_fechaFin.Size = new System.Drawing.Size(98, 16);
            this.lb_fechaFin.TabIndex = 5;
            this.lb_fechaFin.Text = "Fecha fin";
            this.lb_fechaFin.Click += new System.EventHandler(this.label1_Click_3);
            // 
            // dateTime_fin
            // 
            this.dateTime_fin.Location = new System.Drawing.Point(34, 169);
            this.dateTime_fin.Name = "dateTime_fin";
            this.dateTime_fin.Size = new System.Drawing.Size(200, 20);
            this.dateTime_fin.TabIndex = 4;
            this.dateTime_fin.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // lb_fechaInicio
            // 
            this.lb_fechaInicio.AutoSize = true;
            this.lb_fechaInicio.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_fechaInicio.Location = new System.Drawing.Point(31, 92);
            this.lb_fechaInicio.Name = "lb_fechaInicio";
            this.lb_fechaInicio.Size = new System.Drawing.Size(128, 16);
            this.lb_fechaInicio.TabIndex = 3;
            this.lb_fechaInicio.Text = "Fecha inicio";
            this.lb_fechaInicio.Click += new System.EventHandler(this.label1_Click_2);
            // 
            // dateTime_inicio
            // 
            this.dateTime_inicio.Location = new System.Drawing.Point(34, 114);
            this.dateTime_inicio.Name = "dateTime_inicio";
            this.dateTime_inicio.Size = new System.Drawing.Size(200, 20);
            this.dateTime_inicio.TabIndex = 2;
            // 
            // tb_nombreExamen
            // 
            this.tb_nombreExamen.Location = new System.Drawing.Point(34, 63);
            this.tb_nombreExamen.Name = "tb_nombreExamen";
            this.tb_nombreExamen.Size = new System.Drawing.Size(352, 20);
            this.tb_nombreExamen.TabIndex = 1;
            // 
            // lb_nombreExamen
            // 
            this.lb_nombreExamen.AutoSize = true;
            this.lb_nombreExamen.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_nombreExamen.Location = new System.Drawing.Point(31, 44);
            this.lb_nombreExamen.Name = "lb_nombreExamen";
            this.lb_nombreExamen.Size = new System.Drawing.Size(178, 16);
            this.lb_nombreExamen.TabIndex = 0;
            this.lb_nombreExamen.Text = "Nombre del examen";
            // 
            // panel_mtro_capturar
            // 
            this.panel_mtro_capturar.Controls.Add(this.bt_capturar_cancelar);
            this.panel_mtro_capturar.Controls.Add(this.bt_capturar_crearExam);
            this.panel_mtro_capturar.Controls.Add(this.select_correcta);
            this.panel_mtro_capturar.Controls.Add(this.lb_captura_correcta);
            this.panel_mtro_capturar.Controls.Add(this.tb_captura_num);
            this.panel_mtro_capturar.Controls.Add(this.lb_captura_num);
            this.panel_mtro_capturar.Controls.Add(this.bt_capturar_guard);
            this.panel_mtro_capturar.Controls.Add(this.lb_captura_opD);
            this.panel_mtro_capturar.Controls.Add(this.tb_captura_opD);
            this.panel_mtro_capturar.Controls.Add(this.lb_captura_opC);
            this.panel_mtro_capturar.Controls.Add(this.tb_captura_opC);
            this.panel_mtro_capturar.Controls.Add(this.lb_captura_opB);
            this.panel_mtro_capturar.Controls.Add(this.tb_captura_opB);
            this.panel_mtro_capturar.Controls.Add(this.lb_captura_opA);
            this.panel_mtro_capturar.Controls.Add(this.tB_captura_opA);
            this.panel_mtro_capturar.Controls.Add(this.lb_captura_preg);
            this.panel_mtro_capturar.Controls.Add(this.tB_captura_preg);
            this.panel_mtro_capturar.Location = new System.Drawing.Point(240, 10);
            this.panel_mtro_capturar.Name = "panel_mtro_capturar";
            this.panel_mtro_capturar.Size = new System.Drawing.Size(521, 375);
            this.panel_mtro_capturar.TabIndex = 2;
            this.panel_mtro_capturar.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_mtro_capturar_Paint);
            // 
            // select_correcta
            // 
            this.select_correcta.FormattingEnabled = true;
            this.select_correcta.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D"});
            this.select_correcta.Location = new System.Drawing.Point(150, 251);
            this.select_correcta.Name = "select_correcta";
            this.select_correcta.Size = new System.Drawing.Size(121, 21);
            this.select_correcta.TabIndex = 13;
            // 
            // lb_captura_correcta
            // 
            this.lb_captura_correcta.AutoSize = true;
            this.lb_captura_correcta.Location = new System.Drawing.Point(53, 254);
            this.lb_captura_correcta.Name = "lb_captura_correcta";
            this.lb_captura_correcta.Size = new System.Drawing.Size(80, 13);
            this.lb_captura_correcta.TabIndex = 12;
            this.lb_captura_correcta.Text = "Inciso correcto:";
            // 
            // tb_captura_num
            // 
            this.tb_captura_num.Location = new System.Drawing.Point(433, 38);
            this.tb_captura_num.Name = "tb_captura_num";
            this.tb_captura_num.Size = new System.Drawing.Size(54, 20);
            this.tb_captura_num.TabIndex = 11;
            // 
            // lb_captura_num
            // 
            this.lb_captura_num.AutoSize = true;
            this.lb_captura_num.Location = new System.Drawing.Point(329, 38);
            this.lb_captura_num.Name = "lb_captura_num";
            this.lb_captura_num.Size = new System.Drawing.Size(97, 13);
            this.lb_captura_num.TabIndex = 10;
            this.lb_captura_num.Text = "Num de preguntas:";
            this.lb_captura_num.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // bt_capturar_guard
            // 
            this.bt_capturar_guard.BackColor = System.Drawing.Color.Blue;
            this.bt_capturar_guard.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_capturar_guard.ForeColor = System.Drawing.Color.White;
            this.bt_capturar_guard.Location = new System.Drawing.Point(56, 304);
            this.bt_capturar_guard.Name = "bt_capturar_guard";
            this.bt_capturar_guard.Size = new System.Drawing.Size(120, 44);
            this.bt_capturar_guard.TabIndex = 4;
            this.bt_capturar_guard.Text = "Guardar";
            this.bt_capturar_guard.UseVisualStyleBackColor = false;
            this.bt_capturar_guard.Click += new System.EventHandler(this.button_capturar_guard_Click);
            // 
            // lb_captura_opD
            // 
            this.lb_captura_opD.AutoSize = true;
            this.lb_captura_opD.Location = new System.Drawing.Point(53, 220);
            this.lb_captura_opD.Name = "lb_captura_opD";
            this.lb_captura_opD.Size = new System.Drawing.Size(55, 13);
            this.lb_captura_opD.TabIndex = 9;
            this.lb_captura_opD.Text = "Opcion D:";
            // 
            // tb_captura_opD
            // 
            this.tb_captura_opD.Location = new System.Drawing.Point(121, 217);
            this.tb_captura_opD.Name = "tb_captura_opD";
            this.tb_captura_opD.Size = new System.Drawing.Size(366, 20);
            this.tb_captura_opD.TabIndex = 8;
            // 
            // lb_captura_opC
            // 
            this.lb_captura_opC.AutoSize = true;
            this.lb_captura_opC.Location = new System.Drawing.Point(53, 184);
            this.lb_captura_opC.Name = "lb_captura_opC";
            this.lb_captura_opC.Size = new System.Drawing.Size(54, 13);
            this.lb_captura_opC.TabIndex = 7;
            this.lb_captura_opC.Text = "Opcion C:";
            // 
            // tb_captura_opC
            // 
            this.tb_captura_opC.Location = new System.Drawing.Point(121, 181);
            this.tb_captura_opC.Name = "tb_captura_opC";
            this.tb_captura_opC.Size = new System.Drawing.Size(366, 20);
            this.tb_captura_opC.TabIndex = 6;
            // 
            // lb_captura_opB
            // 
            this.lb_captura_opB.AutoSize = true;
            this.lb_captura_opB.Location = new System.Drawing.Point(53, 150);
            this.lb_captura_opB.Name = "lb_captura_opB";
            this.lb_captura_opB.Size = new System.Drawing.Size(54, 13);
            this.lb_captura_opB.TabIndex = 5;
            this.lb_captura_opB.Text = "Opcion B:";
            // 
            // tb_captura_opB
            // 
            this.tb_captura_opB.Location = new System.Drawing.Point(121, 147);
            this.tb_captura_opB.Name = "tb_captura_opB";
            this.tb_captura_opB.Size = new System.Drawing.Size(366, 20);
            this.tb_captura_opB.TabIndex = 4;
            // 
            // lb_captura_opA
            // 
            this.lb_captura_opA.AutoSize = true;
            this.lb_captura_opA.Location = new System.Drawing.Point(53, 114);
            this.lb_captura_opA.Name = "lb_captura_opA";
            this.lb_captura_opA.Size = new System.Drawing.Size(54, 13);
            this.lb_captura_opA.TabIndex = 3;
            this.lb_captura_opA.Text = "Opcion A:";
            this.lb_captura_opA.Click += new System.EventHandler(this.label1_Click);
            // 
            // tB_captura_opA
            // 
            this.tB_captura_opA.Location = new System.Drawing.Point(121, 111);
            this.tB_captura_opA.Name = "tB_captura_opA";
            this.tB_captura_opA.Size = new System.Drawing.Size(366, 20);
            this.tB_captura_opA.TabIndex = 2;
            this.tB_captura_opA.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lb_captura_preg
            // 
            this.lb_captura_preg.AutoSize = true;
            this.lb_captura_preg.Location = new System.Drawing.Point(53, 72);
            this.lb_captura_preg.Name = "lb_captura_preg";
            this.lb_captura_preg.Size = new System.Drawing.Size(53, 13);
            this.lb_captura_preg.TabIndex = 1;
            this.lb_captura_preg.Text = "Pregunta:";
            // 
            // tB_captura_preg
            // 
            this.tB_captura_preg.Location = new System.Drawing.Point(121, 69);
            this.tB_captura_preg.Name = "tB_captura_preg";
            this.tB_captura_preg.Size = new System.Drawing.Size(366, 20);
            this.tB_captura_preg.TabIndex = 0;
            // 
            // panel_mtro_izq
            // 
            this.panel_mtro_izq.Controls.Add(this.pictureBox1);
            this.panel_mtro_izq.Controls.Add(this.button_logout);
            this.panel_mtro_izq.Controls.Add(this.Button_revisar);
            this.panel_mtro_izq.Controls.Add(this.Button_examen);
            this.panel_mtro_izq.Location = new System.Drawing.Point(2, 3);
            this.panel_mtro_izq.Name = "panel_mtro_izq";
            this.panel_mtro_izq.Size = new System.Drawing.Size(229, 401);
            this.panel_mtro_izq.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(64, 37);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 107);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // button_logout
            // 
            this.button_logout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_logout.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_logout.ForeColor = System.Drawing.Color.White;
            this.button_logout.Location = new System.Drawing.Point(64, 324);
            this.button_logout.Name = "button_logout";
            this.button_logout.Size = new System.Drawing.Size(120, 31);
            this.button_logout.TabIndex = 2;
            this.button_logout.Text = "Log out";
            this.button_logout.UseVisualStyleBackColor = false;
            this.button_logout.Click += new System.EventHandler(this.Button_logout_Click);
            // 
            // Button_revisar
            // 
            this.Button_revisar.BackColor = System.Drawing.Color.Blue;
            this.Button_revisar.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_revisar.ForeColor = System.Drawing.Color.White;
            this.Button_revisar.Location = new System.Drawing.Point(64, 243);
            this.Button_revisar.Name = "Button_revisar";
            this.Button_revisar.Size = new System.Drawing.Size(120, 31);
            this.Button_revisar.TabIndex = 1;
            this.Button_revisar.Text = "Revisar";
            this.Button_revisar.UseVisualStyleBackColor = false;
            this.Button_revisar.Click += new System.EventHandler(this.Button_revisar_Click);
            // 
            // Button_examen
            // 
            this.Button_examen.BackColor = System.Drawing.Color.Blue;
            this.Button_examen.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_examen.ForeColor = System.Drawing.Color.White;
            this.Button_examen.Location = new System.Drawing.Point(64, 188);
            this.Button_examen.Name = "Button_examen";
            this.Button_examen.Size = new System.Drawing.Size(120, 31);
            this.Button_examen.TabIndex = 0;
            this.Button_examen.Text = "Examen";
            this.Button_examen.UseVisualStyleBackColor = false;
            this.Button_examen.Click += new System.EventHandler(this.Button_examen_Click);
            // 
            // bt_guardarExamen_archivo
            // 
            this.bt_guardarExamen_archivo.BackColor = System.Drawing.Color.Blue;
            this.bt_guardarExamen_archivo.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_guardarExamen_archivo.ForeColor = System.Drawing.Color.White;
            this.bt_guardarExamen_archivo.Location = new System.Drawing.Point(56, 220);
            this.bt_guardarExamen_archivo.Name = "bt_guardarExamen_archivo";
            this.bt_guardarExamen_archivo.Size = new System.Drawing.Size(162, 47);
            this.bt_guardarExamen_archivo.TabIndex = 4;
            this.bt_guardarExamen_archivo.Text = "Crear examen";
            this.bt_guardarExamen_archivo.UseVisualStyleBackColor = false;
            // 
            // bt_cancelar_archivo
            // 
            this.bt_cancelar_archivo.BackColor = System.Drawing.Color.Blue;
            this.bt_cancelar_archivo.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_cancelar_archivo.ForeColor = System.Drawing.Color.White;
            this.bt_cancelar_archivo.Location = new System.Drawing.Point(251, 220);
            this.bt_cancelar_archivo.Name = "bt_cancelar_archivo";
            this.bt_cancelar_archivo.Size = new System.Drawing.Size(162, 47);
            this.bt_cancelar_archivo.TabIndex = 5;
            this.bt_cancelar_archivo.Text = "Cancelar";
            this.bt_cancelar_archivo.UseVisualStyleBackColor = false;
            this.bt_cancelar_archivo.Click += new System.EventHandler(this.bt_cancelar_archivo_Click);
            // 
            // bt_capturar_crearExam
            // 
            this.bt_capturar_crearExam.BackColor = System.Drawing.Color.Blue;
            this.bt_capturar_crearExam.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_capturar_crearExam.ForeColor = System.Drawing.Color.White;
            this.bt_capturar_crearExam.Location = new System.Drawing.Point(222, 304);
            this.bt_capturar_crearExam.Name = "bt_capturar_crearExam";
            this.bt_capturar_crearExam.Size = new System.Drawing.Size(120, 44);
            this.bt_capturar_crearExam.TabIndex = 14;
            this.bt_capturar_crearExam.Text = "Crear Examen";
            this.bt_capturar_crearExam.UseVisualStyleBackColor = false;
            // 
            // bt_capturar_cancelar
            // 
            this.bt_capturar_cancelar.BackColor = System.Drawing.Color.Blue;
            this.bt_capturar_cancelar.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_capturar_cancelar.ForeColor = System.Drawing.Color.White;
            this.bt_capturar_cancelar.Location = new System.Drawing.Point(367, 304);
            this.bt_capturar_cancelar.Name = "bt_capturar_cancelar";
            this.bt_capturar_cancelar.Size = new System.Drawing.Size(120, 44);
            this.bt_capturar_cancelar.TabIndex = 15;
            this.bt_capturar_cancelar.Text = "Cancelar";
            this.bt_capturar_cancelar.UseVisualStyleBackColor = false;
            this.bt_capturar_cancelar.Click += new System.EventHandler(this.bt_capturar_cancelar_Click);
            // 
            // MaestroForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 409);
            this.Controls.Add(this.panel_mtro_izq);
            this.Controls.Add(this.panel_mtro_archivo);
            this.Controls.Add(this.panel_capturar);
            this.Controls.Add(this.panel_mtro_capturar);
            this.Name = "MaestroForm";
            this.Text = "Maestro";
            this.panel_mtro_archivo.ResumeLayout(false);
            this.panel_mtro_archivo.PerformLayout();
            this.panel_capturar.ResumeLayout(false);
            this.panel_capturar.PerformLayout();
            this.panel_mtro_capturar.ResumeLayout(false);
            this.panel_mtro_capturar.PerformLayout();
            this.panel_mtro_izq.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_mtro_archivo;
        private System.Windows.Forms.Panel panel_mtro_izq;
        private System.Windows.Forms.Button Button_examen;
        private System.Windows.Forms.Button button_logout;
        private System.Windows.Forms.Button Button_revisar;
        private System.Windows.Forms.Label lb_numPreg;
        private System.Windows.Forms.TextBox tB_numPreg;
        private System.Windows.Forms.Panel panel_mtro_capturar;
        private System.Windows.Forms.Label lb_captura_opA;
        private System.Windows.Forms.TextBox tB_captura_opA;
        private System.Windows.Forms.Label lb_captura_preg;
        private System.Windows.Forms.TextBox tB_captura_preg;
        private System.Windows.Forms.Button bt_capturar_guard;
        private System.Windows.Forms.Label lb_captura_opD;
        private System.Windows.Forms.TextBox tb_captura_opD;
        private System.Windows.Forms.Label lb_captura_opC;
        private System.Windows.Forms.TextBox tb_captura_opC;
        private System.Windows.Forms.Label lb_captura_opB;
        private System.Windows.Forms.TextBox tb_captura_opB;
        private System.Windows.Forms.Label lb_captura_num;
        private System.Windows.Forms.TextBox tb_captura_num;
        private System.Windows.Forms.Button bt_capturar;
        private System.Windows.Forms.Label lb_captura_correcta;
        private System.Windows.Forms.ComboBox select_correcta;
        private System.Windows.Forms.Panel panel_capturar;
        private System.Windows.Forms.Label lb_fechaInicio;
        private System.Windows.Forms.DateTimePicker dateTime_inicio;
        private System.Windows.Forms.TextBox tb_nombreExamen;
        private System.Windows.Forms.Label lb_nombreExamen;
        private System.Windows.Forms.Label lb_fechaFin;
        private System.Windows.Forms.DateTimePicker dateTime_fin;
        private System.Windows.Forms.Button bt_seleccionarArchivo;
        public System.Windows.Forms.Button bt_Archivo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button bt_cancelar_archivo;
        private System.Windows.Forms.Button bt_guardarExamen_archivo;
        private System.Windows.Forms.Button bt_capturar_crearExam;
        private System.Windows.Forms.Button bt_capturar_cancelar;
    }
}