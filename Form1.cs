﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamenesAuto
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            panelAlumno.Visible = false;
            panelMaestro.Visible = false;
        }

        private void select_tipoUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(select_tipoUser.SelectedIndex == 0)
            {
                panelMaestro.Visible = true;
                panelAlumno.Visible = false;
            }
            else if(select_tipoUser.SelectedIndex == 1)
            {
                panelMaestro.Visible = false;
                panelAlumno.Visible = true;
            }
        }

        private void button_login_alum_Click(object sender, EventArgs e)
        {
            AlumnoForm af = new AlumnoForm();
            af.Show();
            this.Hide();
        }

        private void button_login_mtro_Click(object sender, EventArgs e)
        {
            MaestroForm mf = new MaestroForm();
            mf.Show();
            this.Hide();
        }
    }
}
