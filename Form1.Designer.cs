﻿namespace ExamenesAuto
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.select_tipoUser = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_user_mtro = new System.Windows.Forms.TextBox();
            this.textBox_pass_mtro = new System.Windows.Forms.TextBox();
            this.button_login_mtro = new System.Windows.Forms.Button();
            this.panelMaestro = new System.Windows.Forms.Panel();
            this.panelAlumno = new System.Windows.Forms.Panel();
            this.button_login_alum = new System.Windows.Forms.Button();
            this.textBox_pass_alum = new System.Windows.Forms.TextBox();
            this.textBox_user_alum = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panelMaestro.SuspendLayout();
            this.panelAlumno.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Console", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(37, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(715, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sistema para Examenes Automatizados FEI";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // select_tipoUser
            // 
            this.select_tipoUser.FormattingEnabled = true;
            this.select_tipoUser.Items.AddRange(new object[] {
            "Maestro",
            "Alumno"});
            this.select_tipoUser.Location = new System.Drawing.Point(339, 121);
            this.select_tipoUser.Name = "select_tipoUser";
            this.select_tipoUser.Size = new System.Drawing.Size(130, 21);
            this.select_tipoUser.TabIndex = 1;
            this.select_tipoUser.SelectedIndexChanged += new System.EventHandler(this.select_tipoUser_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(42, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Iniciar sesion";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(32, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 18);
            this.label4.TabIndex = 2;
            this.label4.Text = "Usuario";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(32, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 18);
            this.label5.TabIndex = 3;
            this.label5.Text = "Contraseña";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // textBox_user_mtro
            // 
            this.textBox_user_mtro.Location = new System.Drawing.Point(35, 66);
            this.textBox_user_mtro.Name = "textBox_user_mtro";
            this.textBox_user_mtro.Size = new System.Drawing.Size(100, 20);
            this.textBox_user_mtro.TabIndex = 4;
            // 
            // textBox_pass_mtro
            // 
            this.textBox_pass_mtro.Location = new System.Drawing.Point(35, 124);
            this.textBox_pass_mtro.Name = "textBox_pass_mtro";
            this.textBox_pass_mtro.PasswordChar = '*';
            this.textBox_pass_mtro.Size = new System.Drawing.Size(100, 20);
            this.textBox_pass_mtro.TabIndex = 5;
            // 
            // button_login_mtro
            // 
            this.button_login_mtro.BackColor = System.Drawing.Color.Blue;
            this.button_login_mtro.ForeColor = System.Drawing.Color.White;
            this.button_login_mtro.Location = new System.Drawing.Point(56, 177);
            this.button_login_mtro.Name = "button_login_mtro";
            this.button_login_mtro.Size = new System.Drawing.Size(76, 38);
            this.button_login_mtro.TabIndex = 6;
            this.button_login_mtro.Text = "Entrar";
            this.button_login_mtro.UseVisualStyleBackColor = false;
            this.button_login_mtro.Click += new System.EventHandler(this.button_login_mtro_Click);
            // 
            // panelMaestro
            // 
            this.panelMaestro.Controls.Add(this.button_login_mtro);
            this.panelMaestro.Controls.Add(this.textBox_pass_mtro);
            this.panelMaestro.Controls.Add(this.textBox_user_mtro);
            this.panelMaestro.Controls.Add(this.label5);
            this.panelMaestro.Controls.Add(this.label4);
            this.panelMaestro.Controls.Add(this.label2);
            this.panelMaestro.Location = new System.Drawing.Point(304, 159);
            this.panelMaestro.Name = "panelMaestro";
            this.panelMaestro.Size = new System.Drawing.Size(202, 228);
            this.panelMaestro.TabIndex = 2;
            // 
            // panelAlumno
            // 
            this.panelAlumno.Controls.Add(this.button_login_alum);
            this.panelAlumno.Controls.Add(this.textBox_pass_alum);
            this.panelAlumno.Controls.Add(this.textBox_user_alum);
            this.panelAlumno.Controls.Add(this.label3);
            this.panelAlumno.Controls.Add(this.label6);
            this.panelAlumno.Controls.Add(this.label7);
            this.panelAlumno.Location = new System.Drawing.Point(304, 159);
            this.panelAlumno.Name = "panelAlumno";
            this.panelAlumno.Size = new System.Drawing.Size(202, 228);
            this.panelAlumno.TabIndex = 3;
            // 
            // button_login_alum
            // 
            this.button_login_alum.BackColor = System.Drawing.Color.Blue;
            this.button_login_alum.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_login_alum.ForeColor = System.Drawing.Color.White;
            this.button_login_alum.Location = new System.Drawing.Point(59, 177);
            this.button_login_alum.Name = "button_login_alum";
            this.button_login_alum.Size = new System.Drawing.Size(76, 38);
            this.button_login_alum.TabIndex = 12;
            this.button_login_alum.Text = "Entrar";
            this.button_login_alum.UseVisualStyleBackColor = false;
            this.button_login_alum.Click += new System.EventHandler(this.button_login_alum_Click);
            // 
            // textBox_pass_alum
            // 
            this.textBox_pass_alum.Location = new System.Drawing.Point(38, 124);
            this.textBox_pass_alum.Name = "textBox_pass_alum";
            this.textBox_pass_alum.PasswordChar = '*';
            this.textBox_pass_alum.Size = new System.Drawing.Size(100, 20);
            this.textBox_pass_alum.TabIndex = 11;
            // 
            // textBox_user_alum
            // 
            this.textBox_user_alum.Location = new System.Drawing.Point(38, 66);
            this.textBox_user_alum.Name = "textBox_user_alum";
            this.textBox_user_alum.Size = new System.Drawing.Size(100, 20);
            this.textBox_user_alum.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(35, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 18);
            this.label3.TabIndex = 9;
            this.label3.Text = "Contraseña";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(35, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 18);
            this.label6.TabIndex = 8;
            this.label6.Text = "Usuario";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(45, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 18);
            this.label7.TabIndex = 7;
            this.label7.Text = "Iniciar sesion";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panelAlumno);
            this.Controls.Add(this.panelMaestro);
            this.Controls.Add(this.select_tipoUser);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sistema de Examenes Automatizado";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelMaestro.ResumeLayout(false);
            this.panelMaestro.PerformLayout();
            this.panelAlumno.ResumeLayout(false);
            this.panelAlumno.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox select_tipoUser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_user_mtro;
        private System.Windows.Forms.TextBox textBox_pass_mtro;
        private System.Windows.Forms.Button button_login_mtro;
        private System.Windows.Forms.Panel panelMaestro;
        private System.Windows.Forms.Panel panelAlumno;
        private System.Windows.Forms.Button button_login_alum;
        private System.Windows.Forms.TextBox textBox_pass_alum;
        private System.Windows.Forms.TextBox textBox_user_alum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}

