﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamenesAuto
{
    public partial class MaestroForm : Form
    {
        private List<Pregunta> banco = new List<Pregunta>();

        public MaestroForm()
        {
            InitializeComponent();
            panel_mtro_archivo.Visible = false;
            panel_mtro_capturar.Visible = false;
            panel_capturar.Visible = false;
        }

        private void Button_logout_Click(object sender, EventArgs e) => Application.Exit();

        private void Button_examen_Click(object sender, EventArgs e)
        {
            panel_capturar.Visible = true;
            panel_mtro_archivo.Visible = false;
            panel_mtro_capturar.Visible = false;
            
            
        
            
        }

        private void Button_capturar_Click(object sender, EventArgs e)
        {
            panel_mtro_capturar.Visible = true;
            panel_mtro_archivo.Visible = false;       
            panel_capturar.Visible = false;
                       
            
            //tb_captura_num.Text = banco.Count().ToString();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }



 

        private void button_capturar_guard_Click(object sender, EventArgs e)
        {
            banco.Add(new Pregunta(tB_captura_preg.Text, tB_captura_opA.Text, tb_captura_opB.Text, tb_captura_opC.Text, tb_captura_opD.Text, select_correcta.SelectedIndex));
            tB_captura_preg.Clear();
            tB_captura_opA.Clear();
            tb_captura_opB.Clear();
            tb_captura_opC.Clear();
            tb_captura_opD.Clear();
            select_correcta.ResetText();
            tb_captura_num.Text = banco.Count().ToString();

        }

        private void Button_revisar_Click(object sender, EventArgs e)
        {
            panel_mtro_capturar.Visible = false;
            panel_mtro_archivo.Visible = false;
            panel_capturar.Visible = false;
        }   

        private void bt_Archivo_Click(object sender, EventArgs e)
        {
            panel_mtro_archivo.Visible = true;
            panel_mtro_capturar.Visible = false;
            panel_capturar.Visible = false;
        }
        
        private void label1_Click_2(object sender, EventArgs e)
        {

        }
        
        private void label1_Click_3(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }        
        
        private void panel_mtro_capturar_Paint(object sender, PaintEventArgs e)
        {
           
            
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void bt_seleccionarArchivo_Click(object sender, EventArgs e)
        {
          
         using(var reader = new System.IO.StreamReader(@"C:\Users\oscal\Desktop\preg2.csv"))
         {
             List<Pregunta> banco = new List<Pregunta>();
             while (!reader.EndOfStream)
             {
                 var line = reader.ReadLine();
                 string[] values = line.Split(',');                    
                 banco.Add(new Pregunta(values[1], values[2], values[3], values[4], values[5], Int16.Parse(values[6])));
             }
             tB_numPreg.Text = banco.Count().ToString();
         }
        }

        private void bt_capturar_cancelar_Click(object sender, EventArgs e)
        {
            panel_mtro_archivo.Visible = false;
            panel_mtro_capturar.Visible = false;
            panel_capturar.Visible = false;
        }

        private void bt_cancelar_archivo_Click(object sender, EventArgs e)
        {
            panel_mtro_archivo.Visible = false;
            panel_mtro_capturar.Visible = false;
            panel_capturar.Visible = false;
        }
    }
}
